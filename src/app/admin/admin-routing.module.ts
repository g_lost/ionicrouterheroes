import { ManageHeroesComponent } from './manage-heroes/manage-heroes.component'
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component'
import { ManageCrisesComponent } from './manage-crises/manage-crises.component'
import { AdminComponent } from './admin/admin.component'
import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { AuthGuard } from '../auth/auth.guard'

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        canActivateChild: [AuthGuard],
        children: [
          {
            path: '',
            component: AdminDashboardComponent,
            data: {animation: 'dashboard'}
          },
          {
            path: 'crises',
            component: ManageCrisesComponent,
            data: {animation: 'crises'}
          },
          {
            path: 'heroes',
            component: ManageHeroesComponent,
            data: {animation: 'heroes'}
          }
        ]
      }
    ]}
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
