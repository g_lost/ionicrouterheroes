import { Component, OnInit } from '@angular/core'
import {SelectivePreloadingStrategyService} from '../../services/selective-preloading-strategy.service'

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent {
  public readonly modules: string[]

  constructor(preloadedRoutes: SelectivePreloadingStrategyService) {
    this.modules = preloadedRoutes.preloadedModules
  }


}
