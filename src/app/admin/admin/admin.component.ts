import { Component, OnInit } from '@angular/core'
import { RouterOutlet } from '@angular/router'
import { slideInAnimation } from '../../animations'

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  animations: [slideInAnimation]
})
export class AdminComponent {

  public getAnimationData(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation']
  }

}
