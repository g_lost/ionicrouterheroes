import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { AdminRoutingModule } from './admin-routing.module'
import { ComponentModule } from '../components/component.module'
import { IonicModule } from '@ionic/angular'
import { AdminComponent } from './admin/admin.component'
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component'
import { ManageCrisesComponent } from './manage-crises/manage-crises.component'
import { ManageHeroesComponent } from './manage-heroes/manage-heroes.component'

@NgModule({
  imports: [
    ComponentModule,
    IonicModule,
    CommonModule,
    AdminRoutingModule
  ],
  declarations: [AdminComponent, AdminDashboardComponent, ManageCrisesComponent, ManageHeroesComponent]
})
export class AdminModule { }
