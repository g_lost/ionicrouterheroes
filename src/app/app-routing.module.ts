import { AuthGuard } from './auth/auth.guard'
import { NgModule } from '@angular/core'
import {Routes, RouterModule, PreloadAllModules} from '@angular/router'
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component'
import { ComposeMessageComponent } from './components/compose-message/compose-message.component'
import {SelectivePreloadingStrategyService} from './services/selective-preloading-strategy.service'

const routes: Routes = [
  {
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule',
    canLoad: [AuthGuard]
  },
  {
    path: 'crises',
    loadChildren: './crisis-center/crises.module#CrisesModule',
    data:  {
      preload: true
    }
  },
  {
    path: 'compose',
    component: ComposeMessageComponent,
    outlet: 'popup'
  },
  {path: '', redirectTo: '/superheroes', pathMatch: 'full'},
  { path: '**', component: PageNotFoundComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    enableTracing: false,
    preloadingStrategy: SelectivePreloadingStrategyService
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
