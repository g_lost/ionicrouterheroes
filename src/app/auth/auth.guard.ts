import { LoginComponent } from './login/login.component'
import { ModalController } from '@ionic/angular'
import { AuthService } from './auth.service'
import { Injectable } from '@angular/core'
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
  CanActivateChild,
  CanLoad,
  Route
} from '@angular/router'
import { Observable, of, from} from 'rxjs'
import { map, first, tap, skip, skipWhile } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(
    private authService: AuthService,
    private router: Router,
    private modalController: ModalController
  ) {}

  public canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    console.log('Guarded!')
    const url = state.url
    return this.checkLogin(url)
  }

  public canLoad(route: Route): boolean| Observable<boolean> | Promise<boolean> {
    const url = `/${route.path}`
    return this.checkLogin(url)
    }

  public canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean| Observable<boolean> | Promise<boolean> {
    return this.canActivate(route, state)
  }

  private checkLogin(url: string): boolean | Observable<boolean> {
    if (this.authService.isAuthentificated()) {
      return true
    }
    this.authService.redirectUrl = url
    return Observable.create((observer) => {
      this.modalController.create({
        component: LoginComponent
      }).then(val => {
        val.onDidDismiss().then(_val => {
          // TODO (Goost) What happens if modal is dimissed from empty page (e.g. direct URL nagivation?)
          observer.next(this.authService.isAuthentificated())
          observer.complete()
        })
        val.present()
      })
    })
  }
}
