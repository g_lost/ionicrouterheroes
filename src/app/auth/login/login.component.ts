import { AuthService } from './../auth.service'
import { FormGroup, Validators } from '@angular/forms'
import { FormBuilder } from '@angular/forms'
import { Component, OnInit } from '@angular/core'
import { ModalController } from '@ionic/angular'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public readonly loginForm: FormGroup
  public message = ''
  constructor(fb: FormBuilder,
    private authService: AuthService,
    private modalController: ModalController) {
    this.loginForm = fb.group({
      user: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  public logIn() {
    this.loginForm.disable()
    this.message = 'Logging in...'
    this.authService
      .login(this.loginForm.value.user, btoa(this.loginForm.value.password))
      .subscribe(val => {
        if (val) {
          this.message = ''
          this.modalController.dismiss()
        } else {
          this.message = 'Wrong user and/or password!'
          this.loginForm.enable()
        }
      })
  }
}
