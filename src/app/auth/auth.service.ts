import { Injectable } from '@angular/core'
import { Observable, of, BehaviorSubject } from 'rxjs'
import { tap, delay } from 'rxjs/operators'
import { Platform } from '@ionic/angular'
import { Storage } from '@ionic/storage'
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _isLoggedIn = false
  public get isLoggedIn(): boolean {
    return this._isLoggedIn
  }
  public redirectUrl: string

  public readonly authentificationState = new BehaviorSubject(false)

  constructor(platform: Platform, private storage: Storage) {
    platform.ready().then() // TODO Storage
  }

  public login(user: string, password: string): Observable<boolean> {
    return of(
      (() => {
        return user === 'foo' && password === btoa('bar')
      })()
    ).pipe(
      delay(1000),
      tap(val => this._isLoggedIn = val),
      tap(val => this.authentificationState.next(val))
    )
  }

  public isAuthentificated(): boolean {
    return this.authentificationState.value
  }

  public logout(): Observable<boolean> {
    return of(true).pipe(
      delay(2000),
      tap(val => this._isLoggedIn = false),
      tap(val => this.authentificationState.next(false))
    )
  }
}
