import { AdminModule } from './admin/admin.module'
import { CrisesModule } from './crisis-center/crises.module'
import { NgModule } from '@angular/core'
import { RouteReuseStrategy } from '@angular/router'

import { IonicModule, IonicRouteStrategy } from '@ionic/angular'
import { SplashScreen } from '@ionic-native/splash-screen/ngx'
import { StatusBar } from '@ionic-native/status-bar/ngx'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component'
import { HeroesModule } from './heroes/heroes.module'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ComponentModule } from './components/component.module'
import { ComposeMessageComponent } from './components/compose-message/compose-message.component'
import { ReactiveFormsModule } from '@angular/forms'
import { IonicStorageModule } from '@ionic/storage'

@NgModule({
  declarations: [AppComponent, PageNotFoundComponent, ComposeMessageComponent],
  entryComponents: [],
  imports: [
    ReactiveFormsModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    HeroesModule,
    AppRoutingModule,
    ComponentModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
