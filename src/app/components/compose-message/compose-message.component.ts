import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'

@Component({
  selector: 'app-compose-message',
  templateUrl: './compose-message.component.html',
  styleUrls: ['./compose-message.component.scss']
})
export class ComposeMessageComponent {
  public details: string
  public readonly message: FormGroup
  public sending = false
  get eMail() { return this.message.get('eMail') }

  constructor(private router: Router, formBuilder: FormBuilder) {
    this.message = formBuilder.group({
      eMail: ['', [Validators.required, Validators.email]],
      contents: ['', Validators.required]
    })
  }

  public send() {
    this.sending = true
    this.message.disable()
    this.details = `Sending Message...${this.message.value.contents} from ${this.message.value.eMail}`

    setTimeout(() => {
      this.sending = false
      this.closePopup()
    }, 3000)
  }

  public cancel() {
    this.closePopup()
  }

  public closePopup() {
    // Providing a `null` value to the named outlet
    // clears the contents of the named outlet
    this.router.navigate([{ outlets: { popup: null } }])
  }
}
