import { FormControl } from '@angular/forms'
import { CrisesService } from './../../crisis-center/services/crises.service'
import { Component, OnInit, Output, ViewChild } from '@angular/core'
import { PopoverController, NavParams, ModalController } from '@ionic/angular'
import { OverlayBaseController } from '@ionic/angular/dist/util/overlay'

@Component({
  selector: 'app-add-popover',
  templateUrl: './add-popover.component.html',
  styleUrls: ['./add-popover.component.scss']
})
export class AddPopoverComponent implements OnInit {
  public readonly name = new  FormControl('')
  // TODO (goost) Does not work
  @ViewChild('ion-input') public myInput
  private controller: OverlayBaseController<any, any>
  constructor(
    private popController: PopoverController,
    private modalController: ModalController,
    private params: NavParams,
    private crisesService: CrisesService
  ) {

  }

  public ngOnInit() {
    this.controller = this.params.data.isModal ? this.modalController : this.popController
    console.log(`Modal: ${this.params.data.isModal}`)
    // this.myInput.setFocus();
  }

  public async close() {
    if (this.name.value && this.name.value !== '') {
      this.params.data.success(this.name.value)
      // this.crisesService.add(this.name)
    }
    await this.controller.dismiss()
  }
}
