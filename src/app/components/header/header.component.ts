import { ModalController, LoadingController } from '@ionic/angular'
import { ActivatedRoute } from '@angular/router'
import { Observable } from 'rxjs'
import { AuthService } from './../../auth/auth.service'
import { Router } from '@angular/router'
import {Component, Input, OnInit} from '@angular/core'
import { map } from 'rxjs/operators'
import { LoginComponent } from '../../auth/login/login.component'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() public backButton: string

  constructor(
    private router: Router,
    private modalController: ModalController,
    private loadingController: LoadingController,
    private authService: AuthService) { }

  public ngOnInit() {
  }

  public openContact() {
    // TODO (goost) This did not work using a routerLink binding directly on the html-tag!
    // [routerLink]="[{outlets: {popup: ['compose']}}]"
    console.log('Open Contacts!')
    this.router.navigate([{outlets: {popup: ['compose']}}])
  }

  public isLoggedOut(): Observable<boolean> {
    return this.authService.authentificationState.pipe(
      map(v => !v)
    )
  }

  public async logIn() {
    const modal = await this.modalController.create({
      component: LoginComponent
    })
    return await modal.present()
  }

  public async logOut() {
    const spinners = ['crescent', 'lines', 'dots', 'bubbles', 'circles']
    const spinner = spinners[Math.trunc(Math.random() * spinners.length)]
    const loading = await this.loadingController.create({
      spinner: spinner
    })
    this.authService.logout().subscribe(val => {
      loading.dismiss()
      this.router.navigate(['/heroes'])
    })
    await loading.present()
  }

}
