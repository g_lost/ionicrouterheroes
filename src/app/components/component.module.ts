import { LoginComponent } from './../auth/login/login.component'
import { HeaderComponent } from './header/header.component'
import { IonicModule } from '@ionic/angular'
import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { AddPopoverComponent } from './add-popover/add-popover.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    IonicModule
  ],
  declarations: [HeaderComponent, AddPopoverComponent, LoginComponent],
  exports: [HeaderComponent, AddPopoverComponent],
  entryComponents: [AddPopoverComponent, LoginComponent]
})
export class ComponentModule { }
