import { HeroDetailComponent } from './hero-detail/hero-detail.component'
import { HeroListComponent } from './hero-list/hero-list.component'
import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

const routes: Routes = [
  {path: 'heroes', redirectTo: '/superheroes' },
  {path: 'hero/:id', redirectTo: '/superheroes/:id' },
  {path: 'superheroes', component: HeroListComponent},
  {path: 'superheroes/:id', component: HeroDetailComponent}
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HeroesRoutingModule { }
