import { ComponentModule } from './../components/component.module'
import { HeaderComponent } from './../components/header/header.component'
import { IonicModule } from '@ionic/angular'
import { HeroListComponent } from './hero-list/hero-list.component'
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { CommonModule } from '@angular/common'

import { HeroesRoutingModule } from './heroes-routing.module'
import { HeroDetailComponent } from './hero-detail/hero-detail.component'
import { FormsModule } from '@angular/forms'

@NgModule({
  imports: [
    ComponentModule,
    FormsModule,
    CommonModule,
    IonicModule,
    HeroesRoutingModule,
  ],
  declarations: [HeroDetailComponent, HeroListComponent],
})
export class HeroesModule { }
