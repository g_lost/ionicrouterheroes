import {HeroService} from './../services/hero.service'
import {Component, OnInit} from '@angular/core'
import {Hero} from '../hero'
import {ActivatedRoute, ParamMap, Router} from '@angular/router'
import {switchMap} from 'rxjs/operators'
import {Observable} from 'rxjs'
import {NavController} from '@ionic/angular'
import {NavIntent} from '@ionic/angular/dist/providers/nav-controller'

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.scss'],
})
export class HeroDetailComponent implements OnInit {
  public hero$: Observable<Hero>

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: HeroService,
    private ionRouter: NavController
  ) {}

  public ngOnInit() {
    this.hero$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.service.getHero(params.get('id')))
    )
  }

  public gotoHeroes(hero: Hero) {
    const heroId = hero ? hero.id : null
    // Pass along the hero id if available
    // so that the HeroList component can select that hero.
    // Include a junk 'foo' property for fun.
    this.ionRouter.navigateBack(['/superheroes', { id: heroId, foo: 'foo' }], true)
    // this.router.navigate(['/heroes', { id: heroId, foo: 'foo' }]);
  }
}
