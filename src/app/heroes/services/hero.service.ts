import { Injectable } from '@angular/core'

import { Observable, of } from 'rxjs'

import { Hero } from './../hero'
import { HEROES } from './../mock-heroes'
import { MessageService } from './../../services/message.service'
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class HeroService {
  constructor(private messageService: MessageService) {}

  public getHeroes(): Observable<Hero[]> {
    // TODO: send the message _after_ fetching the heroes
    this.messageService.add('HeroService: fetched heroes')
    return of(HEROES)
  }

  public getHero(id: number | string): Observable<Hero> {
    return this.getHeroes().pipe(
      map((heroes: Hero[]) => heroes.find(hero => hero.id === +id))
    )
  }

  public add(name: string): void {
    const hero = {id: HEROES[HEROES.length - 1].id + 1, name: name}
    console.log(`Adding crisis: ${hero}`)
    HEROES.push(hero)
  }
}
