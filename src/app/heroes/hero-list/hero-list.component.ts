import { switchMap } from 'rxjs/operators'
import { ActivatedRoute } from '@angular/router'
import { Observable } from 'rxjs'
import { Component, OnInit } from '@angular/core'
import { Hero } from '../hero'
import { HeroService } from '../services/hero.service'
import { ModalController } from '@ionic/angular'
import { AddPopoverComponent } from '../../components/add-popover/add-popover.component'


@Component({
  selector: 'app-hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.scss']
})
export class HeroListComponent implements OnInit {
  public selectedId: number
  public heroes$: Observable<Hero[]>

  constructor(
    private heroService: HeroService,
    private route: ActivatedRoute,
    private modalController: ModalController,
  ) {

  }

  public ngOnInit() {
    this.heroes$ = this.route.paramMap.pipe(
      switchMap(params => {
        this.selectedId = +params.get('id')
        return this.heroService.getHeroes()
      })
    )
  }

  public onSelect(hero: Hero): void {
    this.selectedId = hero.id
  }

  public async createHero() {
    const pop = await this.modalController.create({
      component: AddPopoverComponent,
      componentProps: {
        isModal: true,
        success: (name: string) => this.heroService.add(name)
      }
    })
    return await pop.present()
  }
}
