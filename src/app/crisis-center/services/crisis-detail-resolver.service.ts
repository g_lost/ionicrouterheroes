import { Observable, EMPTY, of } from 'rxjs'
import { Injectable } from '@angular/core'
import { Resolve, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router'
import { Crisis } from '../crisis'
import { CrisesService } from './crises.service'
import { mergeMap, take } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class CrisisDetailResolverService implements Resolve<Crisis> {

  constructor(private criseService: CrisesService, private router: Router) { }

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Crisis> | Observable<never> {
    const id = route.paramMap.get('id')
    console.log(`Resolving crisis with ID ${id}`)
    return this.criseService.getCrisis(id).pipe(
      take(1),
      mergeMap(crisis => {
        if (crisis) {
          return of(crisis)
        } else {
          this.router.navigate(['/crises'])
          return EMPTY
        }
      })
    )
  }


}
