import { Injectable } from '@angular/core'

import { Observable, of } from 'rxjs'

import { Crisis } from './../crisis'
import { CRISES } from './../mock-crises'
import { MessageService } from './../../services/message.service'
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class CrisesService {
  constructor(private messageService: MessageService) {}

  public getCrises(): Observable<Crisis[]> {
    // TODO: send the message _after_ fetching the heroes
    // TODO (goost) We do not really need this messageService....
    this.messageService.add('CrisisService: fetched crises')
    return of(CRISES)
  }

  public getCrisis(id: number | string): Observable<Crisis> {
    return this.getCrises().pipe(
      map((crises: Crisis[]) => crises.find(crisis => crisis.id === +id))
    )
  }

  public delete (crisis: Crisis): void {
    console.log('Delete called!')
      const index = CRISES.indexOf(crisis, 0)
      if (index > -1) {
        CRISES.splice(index, 1)
      }
  }

  public add(name: string): void {
    const crisis = {id: CRISES[CRISES.length - 1].id + 1, name: name}
    console.log(`Adding crisis: ${crisis}`)
    CRISES.push(crisis)
  }
}
