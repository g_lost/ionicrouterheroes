import { CanComponentDeactivate } from './../../services/guards/can-deactivate.guard'
import { CrisesService } from './../services/crises.service'
import { Component, OnInit, Input, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { Crisis } from '../crisis'
import { ActivatedRoute, Router, ParamMap } from '@angular/router'
import { switchMap, tap } from 'rxjs/operators'
import { Observable, of } from 'rxjs'
import { Dialogs } from '@ionic-native/dialogs/ngx'

@Component({
  selector: 'app-crisis-detail',
  templateUrl: './crisis-detail.component.html',
  styleUrls: ['./crisis-detail.component.scss'],
})
export class CrisisDetailComponent implements OnInit, CanComponentDeactivate {
  public crisis: Crisis
  public editName = ''

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: CrisesService,
    private dialogs: Dialogs
  ) {}

  public canDeactivate(): Promise<boolean> | boolean| Observable<boolean> {
    // Allow synchronous navigation (`true`) if no crisis or the crisis is unchanged
    if (!this.crisis || this.crisis.name === this.editName) {
      return true
    }
    // NOTE (goost) Cordova plugin needs cordova in browser (`ionic cordova run browser`), but this breaks angular routing
    // -> Navigating via url does not work anymore
    // return this.dialogs.confirm("Discard Changes?","Confirm", ['OK', 'Cancel']).then(val => val === 1)
    const confirmation = window.confirm('Discard Changes?')
    return of(confirmation)
  }

  public ngOnInit() {
    this.route.data.subscribe((data: {crisis: Crisis}) => {
      this.crisis = data.crisis
      this.editName = data.crisis.name
    })

  }

  public save() {
    this.crisis.name = this.editName
    this.gotoCrises(this.crisis)
  }

  public gotoCrises(crisis: Crisis) {
    const crisisId = crisis ? crisis.id : null
    // Pass along the hero id if available
    // so that the HeroList component can select that hero.
    // Include a junk 'foo' property for fun.
    this.router.navigate(['../', { id: crisisId, foo: 'foo' }], {relativeTo: this.route})
  }
}
