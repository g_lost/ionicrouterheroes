import {switchMap} from 'rxjs/operators'
import {ActivatedRoute} from '@angular/router'
import {Observable} from 'rxjs'
import {Component, OnInit} from '@angular/core'
import {Crisis} from '../crisis'
import {CrisesService} from '../services/crises.service'
import {PopoverController} from '@ionic/angular'
import {AddPopoverComponent} from '../../components/add-popover/add-popover.component'

@Component({
  selector: 'app-crises-list',
  templateUrl: './crises-list.component.html',
  styleUrls: ['./crises-list.component.scss']
})
export class CrisesListComponent implements OnInit {

  public selectedId: number
  public crises$: Observable<Crisis[]>

  constructor(
    private crisesService: CrisesService,
    private route: ActivatedRoute,
    private popoverController: PopoverController) {
  }

  public ngOnInit() {
    this.crises$ = this.route.paramMap.pipe(
      switchMap(params => {
        this.selectedId = +params.get('id')
        return this.crisesService.getCrises()
      })
    )
  }

  public onSelect(ev: Event, crisis: Crisis): void {
    this.selectedId = crisis.id
  }

  public delete(crisis: Crisis) {
    this.crisesService.delete(crisis)
  }

  public async createCrisis(ev) {
    const pop = await this.popoverController.create(
      {
        component: AddPopoverComponent,
        componentProps: {
          success: (name: string) => this.crisesService.add(name)
        },
        event: ev,
      })
    return await pop.present()
  }

}



