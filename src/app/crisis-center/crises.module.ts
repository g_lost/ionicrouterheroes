import { ComponentModule } from './../components/component.module'
import { IonicModule } from '@ionic/angular'
import { CrisesListComponent } from './crises-list/crises-list.component'
import { NgModule} from '@angular/core'
import { CommonModule } from '@angular/common'

import { CrisesRoutingModule } from './crises-routing.module'
import { CrisisDetailComponent } from './crisis-detail/crisis-detail.component'
import { FormsModule } from '@angular/forms'
import { CrisisCenterComponent } from './crisis-center/crisis-center.component'
import { CrisisCenterHomeComponent } from './crisis-center-home/crisis-center-home.component'
import { Dialogs } from '@ionic-native/dialogs/ngx'

@NgModule({
  imports: [
    ComponentModule,
    FormsModule,
    CommonModule,
    IonicModule,
    CrisesRoutingModule,
  ],
  providers: [Dialogs],
  declarations: [CrisisDetailComponent, CrisesListComponent, CrisisCenterComponent, CrisisCenterHomeComponent],
})
export class CrisesModule { }
