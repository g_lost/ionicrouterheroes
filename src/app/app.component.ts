import { Component, ViewChildren, QueryList } from '@angular/core'
import { Platform, IonRouterOutlet, ToastController, MenuController } from '@ionic/angular'
import { SplashScreen } from '@ionic-native/splash-screen/ngx'
import { StatusBar } from '@ionic-native/status-bar/ngx'
import { RouterOutlet, Router } from '@angular/router'

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public routes = [
    {
      name: 'Crisis Center',
      routerLink: '/crises'
    },
    {
      name: 'Heroes',
      routerLink: '/superheroes'
    },
    {
      name: 'Administration',
      routerLink: '/admin'
    }
  ]
  @ViewChildren(IonRouterOutlet) public ionRouterOutlets: QueryList<IonRouterOutlet>
  @ViewChildren(RouterOutlet) public routerOutlets: QueryList<RouterOutlet>

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    toastController: ToastController,
    menuController: MenuController
  ) {
    this.initializeApp(toastController, menuController)
  }

  public initializeApp(toastController: ToastController, menuController: MenuController) {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault()
      this.splashScreen.hide()
      let backPressedOnce = false
      const timePeriodToExit = 2000
      this.ionRouterOutlets.forEach(async outlet => {
        console.warn(outlet.canGoBack())
      })
      this.platform.backButton.subscribe(async (ev: Event) => {
        ev.stopPropagation()
        const menu = await menuController.get(`mainmenu`)
        const isOpen = await menuController.isOpen(`mainmenu`)
        if (menu !== null && isOpen) {
          await menuController.close()
          return
        }

        this.ionRouterOutlets.forEach(async (outlet: IonRouterOutlet) => {
          if (outlet && outlet.canGoBack() && this.router.url !== '/heroes') {
            outlet.pop()
          } else if (backPressedOnce) {
            navigator['app'].exitApp()
          } else if (this.router.url === '/heroes') {
            const toast = await toastController.create({
              message: 'Press back again to exit App?',
              duration: 3000,
              position: 'bottom'
            })
            toast.present()
            backPressedOnce = true
            setTimeout(() => backPressedOnce = false, timePeriodToExit)
          }
        })
      })
    })
  }
}
