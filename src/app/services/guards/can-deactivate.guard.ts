import { Injectable } from '@angular/core'
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate } from '@angular/router'
import { Observable } from 'rxjs'

export interface CanComponentDeactivate {
  canDeactivate: (route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) => Observable<boolean> | Promise<boolean> | boolean
 }

@Injectable({
  providedIn: 'root'
})
export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {
  public canDeactivate(component: CanComponentDeactivate,
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    return component.canDeactivate ? component.canDeactivate(route, state) : true
  }
}
